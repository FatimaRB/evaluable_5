
package evaluable5_fatimarahmati;

/**
* @author Fátima María Rahmatí Barberá
 * fatimarrb@gmail.com
 * Fecha de entrega: 21/01/2021 - 23:55h
 */
public class Contratado extends Empleado {
    
    //Definimos atributo que tiene esta clase.
    private int diasContrato;
    
    //Definimos el constructor de la clase
    public Contratado (String nombre, String apellido, int edad, int idEmpleado, int diasContrato){
        super (nombre, apellido, edad, idEmpleado);
        this.diasContrato = diasContrato;
    }
    
    //Implementamos método importeNomina para Contratado
    @Override
    public int importeNomina(){
        return 1000;
    }
    
    //Definimos getter y setter del atributo de la clase

    public int getDiasContrato() {
        return diasContrato;
    }

    public void setDiasContrato(int diasContrato) {
        this.diasContrato = diasContrato;
    }
    
}
