package evaluable5_fatimarahmati;

/**
 * @author Fátima María Rahmatí Barberá
 * fatimarrb@gmail.com
 * Fecha de entrega: 21/01/2021 - 23:55h
 */
//Definimos que la clase es abstracta y que hereda de Persona

public abstract class Empleado extends Persona {
    
    //Definimos los atributos no heredados
    private int idEmpleado;
    
    //Definimos constructor de clase
    public Empleado (String nombre, String apellido, int edad, int idEmpleado){
        super (nombre, apellido, edad);
        this.idEmpleado = idEmpleado;
        
    }
    
    //Definimos el método abstract importeNomina()
    public abstract int importeNomina();
    

    //Definimos getter y setter de atributos para que puedan ser empleados en las clases que herede. 
    public int getIdEmpleado() {
        return idEmpleado;
    }

    public void setIdEmpleado(int idEmpleado) {
        this.idEmpleado = idEmpleado;
    }

}
