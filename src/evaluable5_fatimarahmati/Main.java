
package evaluable5_fatimarahmati;

/**
 * @author Fátima María Rahmatí Barberá
 * fatimarrb@gmail.com
 * Fecha de entrega: 21/01/2021 - 23:55h
 */
public class Main {
    
    public static void main(String[] args) {
        
        
        Fijo ef1 = new Fijo ("Fátima María", "Rahmatí Barberá", 29, 35596965);
        Fijo ef2 = new Fijo ("Estrella", "Solar Lunar", 30, 44055066);
        Fijo ef3 = new Fijo ("Dolores", "Fuertes Saez", 46, 55066077);
        Fijo ef4 = new Fijo ("Raul", "Era Jiménez", 32, 66077088);
        
        Contratado ef5 = new Contratado ("Pepito", "Grillo Moreno", 42, 11022033, 30);
        Contratado ef6 = new Contratado ("Carmen", "Andrade Gil", 42, 22033044, 60);
        Contratado ef7 = new Contratado ("Luz", "Fuerte Brillante", 37, 33044055, 35);
        Contratado ef8 = new Contratado ("Fernando", "Rojales Oscuro", 57, 001002, 30);
        
        
        
        Plantilla plantilla = new Plantilla();
        
        plantilla.addEmpleado(ef1);
        plantilla.addEmpleado(ef2);
        plantilla.addEmpleado(ef3);
        plantilla.addEmpleado(ef4);
        plantilla.addEmpleado(ef5);
        plantilla.addEmpleado(ef6);
        plantilla.addEmpleado(ef7);
        plantilla.addEmpleado(ef8);
        
        plantilla.imprimirPlantilla();
        
        System.out.print("\n");
        System.out.println("EL importe de las nóminas de los empleados que consta en la plantilla es :" + plantilla.importeTotalNominaEmpleados() + " euros."+"\n");
        
        
        
    }
}
