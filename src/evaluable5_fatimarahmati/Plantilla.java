
package evaluable5_fatimarahmati;
import java.util.ArrayList;
import java.util.Iterator;

/**
* @author Fátima María Rahmatí Barberá
 * fatimarrb@gmail.com
 * Fecha de entrega: 21/01/2021 - 23:55h
 */
public class Plantilla {
    
    //Definimos variables.
    
    private ArrayList <Empleado> listaEmpleados;
    
    //Constructor de la clase
    public Plantilla (){
        this.listaEmpleados = new ArrayList();
    }
    
    //Creamos la función que añadirá los empleados al ArrayList (addEmpleado)
    public void addEmpleado (Empleado empleado){
        listaEmpleados.add(empleado);
    }
    
       
    //Creamos la función imprimirPlantilla(), desde la cual se imprimirán los datos solicitados.
    
    public void imprimirPlantilla(){
        System.out.println("Se procede a mostrar los datos de los empleados existentes en la plantilla:"+"\n");
        
        Iterator <Empleado> sacarporpantalla = listaEmpleados.iterator();
        int cont = 0;
        while (sacarporpantalla.hasNext()){
            Object empleado = sacarporpantalla.next();
            cont++;
            if (empleado instanceof Fijo) {
                Fijo empleado1 = (Fijo) empleado;
                System.out.println(cont + ". Nombre: " + empleado1.getNombre() + " Apellidos: " + empleado1.getApellidos() + " Edad: " + empleado1.getEdad() + " ID Empleado: " +  empleado1.getIdEmpleado() );
                System.out.println("Tipo de este empleado: Fijo. Nómina de este empleado: " + empleado1.importeNomina());
            }
            
           
            if (empleado instanceof Contratado) {
                Contratado empleado1 = (Contratado) empleado;
                System.out.println(cont + ". Nombre: " + empleado1.getNombre() + " Apellidos: " + empleado1.getApellidos() + " Edad: " + empleado1.getEdad() +  " ID Empleado: " +  empleado1.getIdEmpleado() + " Días de contrato: " + empleado1.getDiasContrato());
                System.out.println("Tipo de este empleado: Contratado. Nómina de este empleado: " + empleado1.importeNomina());
            }
        }
    }
    
    //Creamos la función para obtener el total de las nóminas. 
    public float importeTotalNominaEmpleados () {
        
        float nominasTotales = 0;
        
        for (Empleado empleado : listaEmpleados) {
            nominasTotales += empleado.importeNomina();
        }
        
        return nominasTotales;
    }
  
}
    
    
    
