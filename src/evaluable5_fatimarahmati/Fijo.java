
package evaluable5_fatimarahmati;

/**
 * @author Fátima María Rahmatí Barberá
 * fatimarrb@gmail.com
 * Fecha de entrega: 21/01/2021 - 23:55h
 */
public class Fijo extends Empleado{
    
    //No tiene más atributos. 
    
    //Definimos constructor de la clase
    public Fijo (String nombre, String apellido, int edad, int idEmpleado){
        super (nombre, apellido, edad, idEmpleado);
    }

        
    //Implementamos la función abstracta importeNomina, ya que aquí sí tenemos el importe de su nómina. 
    @Override
    public int importeNomina(){
        return 1500;
    }
       
    
}
